#include "nodetree.h"

template <class n_type>
NodeTree<n_type>::NodeTree(int id, QString label, double x, double y, n_type data) : Node<n_type>(id, label, x, y, data) {

}

template <class n_type>
void NodeTree<n_type>::addSon(NodeTree* n){
    sons.push_back(n);
}

template <class n_type>
void NodeTree<n_type>::removeSon(NodeTree* n){
    for(int i = 0; i < sons.size(); i++){
        if(n->equals(sons[i])){
            NodeTree* nAux = sons[i];
            sons[i] = sons[sons.size() - 1];
            sons[sons.size() - 1] = nAux;
            sons.pop_back();

            S--;
            return;
        }
    }
}

template <class n_type>
vector<NodeTree<n_type>*> NodeTree<n_type>::getSons(){
    vector<NodeTree*> aux = sons;
    return aux;
}

template <class n_type>
NodeTree<n_type>* NodeTree<n_type>::getFather(){
    return father;
}

template <class n_type>
void NodeTree<n_type>::setFather(NodeTree* father){
    this->father = father;
}

template <class n_type>
NodeTree<n_type>::~NodeTree() {

}

template class NodeTree<int>;
template class NodeTree<float>;
template class NodeTree<double>;
