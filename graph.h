#ifndef GRAPH_H
#define GRAPH_H

#define SPHERE_RADIUS 10.0
#define RANDOM_RADIUS 30
#define ZERO 0

#include "node.h"
#include "nodegraph.h"
#include "graphicobject.h"
#include "smartobject.h"
#include "mathhelper.h"
#include <vector>
#include <QMouseEvent>

using namespace std;

template <class n_type, class e_type>

class Graph : public GraphicObject
{
    public:
        Graph();
        ~Graph();

        bool addNode(double x, double y, n_type data);
        bool addEdge(bool directional, NodeGraph<n_type,e_type>* start, NodeGraph<n_type,e_type>* end, e_type weight);

        bool deleteNode(double x, double y);
        bool deleteNode(NodeGraph<n_type,e_type>* n);

        void draw(QOpenGLWidget* widget);

        int edges();

        bool hasNode(NodeGraph<n_type,e_type>* n);

        void populate();
        void populate(int N);
        void populate(vector<n_type> data);
        void populate(vector<Point> pos, vector<n_type> data);
        int size();

        NodeGraph<n_type,e_type>* operator[] (int n);
        NodeGraph<n_type,e_type>* operator() (int n,int e);

        vector<QString> print();
        vector<QString> raw();
        void mousePress2D(double x, double y, QMouseEvent* e);
        void mouseMove2D(double x, double y, QMouseEvent* e);

        NodeGraph<n_type,e_type>* selectNode(double x, double y, double radius);
        NodeGraph<n_type,e_type>* getStart();
        NodeGraph<n_type,e_type>* getEnd();

    private:
        int N; // number of nodes
        int M; // number of edges
        vector<NodeGraph<n_type,e_type>*> nodes;
        NodeGraph<n_type,e_type>* start;
        NodeGraph<n_type,e_type>* end;
        NodeGraph<n_type,e_type>* selected;
        int selectedStatus;
};

#endif // GRAPH_H
