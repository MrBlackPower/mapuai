#include "graphicobject.h"

GraphicObject::GraphicObject() : SmartObject(){
    left = 0.0;
    right = 0.0;
    bottom = 0.0;
    top = 0.0;
    nr = 0.0;
    fr = 0.0;
    center = Point(0.0,0.0,0.0);
    base = glGenLists(256);
    color = QColor("gray");
    status = IDLE;
}

GraphicObject::GraphicObject(QColor color) : SmartObject(){
    left = 0.0;
    right = 0.0;
    bottom = 0.0;
    top = 0.0;
    nr = 0.0;
    fr = 0.0;
    center = Point(0.0,0.0,0.0);
    base = glGenLists(256);
    this->color = color;
}

vector<QString> GraphicObject::print(){
    vector<QString> txt;
    QString line;
    line = "******************************************************************************";
    txt.push_back(line);
    line = "* GRAPHIC OBJECT                                                                      *";
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);
    line = getLabel();
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);
    line = "* END OF GRAPHIC OBJECT                                                                      *";
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);

    return txt;
}

vector<QString> GraphicObject::raw(){
    vector<QString> txt;
    QString line;
    line = "# mapUAI DataFile Version 1.0\n";
    txt.push_back(line);
    line = "# mapUAI output\n";
    txt.push_back(line);
    line = "# DATASET POLYDATA\n";
    txt.push_back(line);
    line = getLabel();
    txt.push_back(line);

    return txt;
}

void GraphicObject::draw(QOpenGLWidget* widget){

}

void GraphicObject::mouseMove2D(double x, double y, QMouseEvent* e){

}

void GraphicObject::mousePress2D(double x, double y, QMouseEvent* e){

}

void GraphicObject::adjustBorder(Point p){
    bool changed = false;
    if(p.x < left){
        left = p.x;
        changed = true;
    }

    if(p.y < bottom){
        bottom = p.y;
        changed = true;
    }

    if(p.z < nr){
        nr = p.z;
        changed = true;
    }

    if(p.x > right){
        right = p.x;
        changed = true;
    }

    if(p.y > top){
        top = p.y;
        changed = true;
    }

    if(p.z > fr){
        fr = p.z;
        changed = true;
    }

    if(changed)
        sizeChanged();
}

void GraphicObject::adjustBorder(int SIDE, double delta){
    switch (SIDE) {
        case LEFT:
            if((left + delta) < right)
                left += delta;
                sizeChanged();
            return;
        case RIGHT:
            if((right + delta) > left)
                right += delta;
                sizeChanged();
            return;
        case BOTTOM:
            if((bottom + delta) < top)
                bottom += delta;
                sizeChanged();
            return;
        case TOP:
            if((top + delta) > bottom)
                top += delta;
                sizeChanged();
            return;
        case NEAR:
            if((nr + delta) < fr)
                nr += delta;
                sizeChanged();
            return;
        case FAR:
            if((fr + delta) > nr)
                fr += delta;
                sizeChanged();
            return;
    default:
        return;
    }
}

void GraphicObject::setGraphicSize(Point p){
    setGraphicSize(p,p);
}

void GraphicObject::setGraphicSize(Point leftBottom, Point rightTop){
    double l = leftBottom.x;
    double b = leftBottom.y;
    double n = leftBottom.z;
    double r = rightTop.x;
    double t = rightTop.y;
    double f = rightTop.z;

    if(l > r){
        double aux = r;
        r = l;
        l = aux;
    }

    if(b > t){
        double aux = b;
        b = t;
        t = aux;
    }

    if(n > f){
        double aux = n;
        n = f;
        f = aux;
    }

    left = l;
    right = r;
    top = t;
    bottom = b;
    nr = n;
    fr = f;
    sizeChanged();
}

void GraphicObject::setGraphicSize(vector<Point> p){
    Point lb;
    Point rt;
    lb.x = p[ZERO].x;
    lb.y = p[ZERO].y;
    lb.z = p[ZERO].z;
    rt.x = p[ZERO].x;
    rt.y = p[ZERO].y;
    rt.z = p[ZERO].z;

    for(int i = 1; i < p.size(); i++){
        Point aux = p[i];
        if(aux.x < lb.x)
            lb.x = aux.x;

        if(aux.y < lb.y)
            lb.y = aux.y;

        if(aux.z < lb.z)
            lb.z = aux.z;

        if(aux.x > rt.x)
            rt.x = aux.x;

        if(aux.y > rt.y)
            rt.y = aux.y;

        if(aux.z > rt.z)
            rt.z = aux.z;
    }

    setGraphicSize(lb,rt);
}

GraphicObject::~GraphicObject(){

}

Point* GraphicObject::getCenter(){
    return &center;
}

QColor GraphicObject::getColor(){
    return color;
}

void GraphicObject::setColor(QColor color){
    if(!color.isValid())
        return;

    this->color = color;
}

void GraphicObject::setStatus(int status){
    this->status = status;
    switch (status) {
    case SELECTED:
        color = QColor("#ff34cd");
        break;
    case IDLE:
        color = QColor("#7f1174");
        break;
    case START:
        color = QColor("#2a5a01");
        break;
    case END:
        color = QColor("#78003c");
        break;
    case OPEN:
        color = QColor("#f5cb04");
        break;
    case CLOSED:
        color = QColor("#7f1174");
        break;
    default:
        this->status = IDLE;
        color = QColor("#f15f06");
        break;
    }
}

int GraphicObject::getStatus(){
    return status;
}

double GraphicObject::getWidth(){
    return (right - left);
}

double GraphicObject::getLeft(){
    return left;
}

double GraphicObject::getRight(){
    return right;
}

double GraphicObject::getHeight(){
    return (top - bottom);
}

double GraphicObject::getBottom(){
    return bottom;
}

double GraphicObject::getTop(){
    return top;
}

double GraphicObject::getDistance(){
    return (fr - nr);
}

double GraphicObject::getNear(){
    return nr;
}

double GraphicObject::getFar(){
    return fr;
}

void GraphicObject::drawText(QOpenGLWidget* widget, QString line, Point p){
    drawText(widget,line,p,color);
}

void GraphicObject::drawText(QOpenGLWidget* widget, QString line, Point p, QColor color){
    //It gives the radius and point in relation to the GraphicObject
    //so we have to calculate the point in relation to the widget
    int h = widget->height();
    int w = widget->width();

    float rx = (w/(right - left)) * 0.8;
    float ry = (h/(top - bottom)) * 0.8;
    float r = (rx > ry)? ry : rx;

    p = p - center;
    p = p * r;
    p = p + Point(1,w*0.1,h*0.1);

    // Retrieve last OpenGL color to use as a font color
    GLdouble glColor[4];
    glGetDoublev(GL_CURRENT_COLOR, glColor);
    QColor fontColor = QColor(glColor[0], glColor[1], glColor[2], glColor[3]);

    // Render text
    QPainter painter(widget);
    painter.setPen(color);
    painter.setFont(font);
    painter.drawText(p.x, p.y, line);
    painter.end();
}

void GraphicObject::drawLine(QOpenGLWidget* widget, Point start, Point end){
    drawLine(widget,start,end,color);
}

void GraphicObject::drawLine(QOpenGLWidget* widget, Point start, Point end, QColor color){
    //It gives the radius and point in relation to the GraphicObject
    //so we have to calculate the point in relation to the widget
    int h = widget->height();
    int w = widget->width();

    float rx = (w/(right - left)) * 0.8;
    float ry = (h/(top - bottom)) * 0.8;
    float r = (rx > ry)? ry : rx;

    start = start - center;
    start = start * r;
    start = start + Point(1,w*0.1,h*0.1);

    end = end - center;
    end = end * r;
    end = end + Point(1,w*0.1,h*0.1);

    // Render text
    QPainter painter(widget);
    painter.setPen(color);
    painter.drawLine(QPointF(start.x,start.y),QPointF(end.x,end.y));
    painter.end();
}

void GraphicObject::drawSphere(QOpenGLWidget* widget, float radius, Point p){
    drawSphere(widget,radius,p,color);
}

void GraphicObject::drawSphere(QOpenGLWidget* widget, float radius, Point p, QColor color){
    //It gives the radius and point in relation to the GraphicObject
    //so we have to calculate the point in relation to the widget
    int h = widget->height();
    int w = widget->width();

    float rx = (w/(right - left)) * 0.8;
    float ry = (h/(top - bottom)) * 0.8;
    float r = (rx > ry)? ry : rx;

    p = p - center;
    p = p * r;
    p = p + Point(1,w*0.1,h*0.1);

    // Render text
    QPainter painter(widget);
    painter.setPen(color);
    painter.setBrush(QBrush(color));
    painter.drawEllipse(QPointF(p.x,p.y),radius,radius);
    painter.end();
}

void GraphicObject::sizeChanged(){
    cout << " << GRAPHIC OBJECT SIZE CHANGED >> " << endl;
    cout << " << left :" << left << " - right :" << right << " >> " << endl;
    cout << " << bottom :" << bottom << " - top :" << top << " >> " << endl;
    cout << " << near :" << nr << " - far :" << fr << " >> " << endl;
}
