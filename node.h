#ifndef NODE_H
#define NODE_H

#include "point.h"
#include "graphicobject.h"
#include <vector>
#include <QString>
#include <QColor>

template <class n_type>
class Node : public GraphicObject
{
public:
    Node(int id, QString label, double x, double y, n_type data);
    ~Node();

    Point getPoint();

    int getId();

    QString getLabel();

    void draw(QOpenGLWidget* widget);
    vector<QString> print();
    vector<QString> raw();
    void mousePress2D(double x, double y, QMouseEvent* e);
    void mouseMove2D(double x, double y, QMouseEvent* e);

    bool equals(Node* n);

    n_type data;
private:
    Point p;
    int id;
    QString label;
};

#endif // NODE_H
