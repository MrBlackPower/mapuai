#include "heuristic.h"

template <class n_type, class e_type>
Heuristic<n_type,e_type>::Heuristic(){
    n = "NONE";
}

template <class n_type, class e_type>
QString Heuristic<n_type,e_type>::name(){
    return n;
}

template <class n_type, class e_type>
void Heuristic<n_type,e_type>::setName(QString name){
    n = name;
}

template <class n_type, class e_type>
double Heuristic<n_type,e_type>::calculate(NodeGraph<n_type,e_type>* n1, NodeGraph<n_type,e_type>* n2){
    return 0.0;
}

template class Heuristic<int,int>;
template class Heuristic<float,int>;
template class Heuristic<float,float>;
template class Heuristic<double,int>;
template class Heuristic<double,double>;
