#include "node.h"

template <class n_type>
Node<n_type>::Node(int id, QString label, double x, double y, n_type data) : GraphicObject(QColor("#7f1174")){
    this->id = id;
    this->label = label;
    p = Point(0,x,y);
    this->data = data;
}

template <class n_type>
Point Node<n_type>::getPoint(){
    return p;
}

template <class n_type>
void Node<n_type>::draw(QOpenGLWidget* widget){

}

template <class n_type>
void Node<n_type>::mouseMove2D(double x, double y, QMouseEvent* e){

}

template <class n_type>
void Node<n_type>::mousePress2D(double x, double y, QMouseEvent* e){

}

template <class n_type>
vector<QString> Node<n_type>::print(){
    vector<QString> txt;
    QString line;
    line = ">>";
    txt.push_back(line);
    line = line.asprintf("NODE (%s) <<"
                         , label.toLatin1().data());
    txt.push_back(line);

    line = ">>";
    txt.push_back(line);

    return txt;
}

template <class n_type>
vector<QString> Node<n_type>::raw(){

}

template <class n_type>
int Node<n_type>::getId(){
    return id;
}

template <class n_type>
QString Node<n_type>::getLabel(){
    return label;
}

template <class n_type>
bool Node<n_type>::equals(Node* n){
    if(id == n->getId())
        return true;

    return false;
}

template <class n_type>
Node<n_type>::~Node(){

}

template class Node<int>;
template class Node<float>;
template class Node<double>;
