#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    mapLoaded = false;
    mapIterated = false;
    map = NULL;
    decisionTree = NULL;
    ia = new Depth<int,int>();
    h = EulerHeuristic<int,int>();
    QObject::connect(this,SIGNAL(updateMainCanvas()),ui->OGLMainWidget,SLOT(update()));
    QObject::connect(this,SIGNAL(updateRightCanvas()),ui->OGLRightWidget,SLOT(update()));
}

MainWindow::~MainWindow()
{
    delete ia;
    delete ui;
}

void MainWindow::on_act_graph_new_triggered(){
    if(mapLoaded)
        on_act_graph_purge_triggered();

    map = new Graph<int,int>();

    ia->setGraph(map);
    mapLoaded = true;
    ui->OGLMainWidget->setGraphicObject(map);
    emit updateMainCanvas();
}

void MainWindow::on_act_graph_purge_triggered()
{
    ui->OGLMainWidget->purgeGraphicObect();

    if(map)
        delete map;

    ia->setGraph(NULL);
    mapLoaded = false;
    ui->OGLMainWidget->purgeGraphicObect();
    emit updateMainCanvas();
}

void MainWindow::on_act_populate_random_triggered()
{
    if(!mapLoaded)
        return;

    map->populate();

    for(int i = 0; i < map->size(); i++){
        for(int j = 0; j < map->size(); j++){
            NodeGraph<int,int>* n1 = map->operator [](i);
            NodeGraph<int,int>* n2 = map->operator [](j);
            map->addEdge(true,n1,n2,1);
        }
    }

    map->printOnConsole();
    emit updateMainCanvas();
}

void MainWindow::on_act_square_case_triggered()
{
    if(!mapLoaded)
        return;

    map->addNode(1.0,1.0,0.0);
    map->addNode(0.0,1.0,0.0);
    map->addNode(0.0,0.0,0.0);
    map->addNode(1.0,0.0,0.0);

    for(int i = 0; i < map->size(); i++){
        for(int j = 0; j < map->size(); j++){
            NodeGraph<int,int>* n1 = map->operator [](i);
            NodeGraph<int,int>* n2 = map->operator [](j);
            map->addEdge(true,n1,n2,1);
        }
    }

    map->printOnConsole();
    emit updateMainCanvas();
}

void MainWindow::on_act_SA_EulerHeuristic_triggered()
{
    bool opt = ui->act_SA_EulerHeuristic->isChecked();
    ui->atc_SA_Manhattan->setChecked(!opt);

    if(opt){
        h = EulerHeuristic<int,int>();
    } else {
        h = ManhattanHeuristic<int,int>();
    }
}

void MainWindow::on_atc_SA_Manhattan_triggered()
{
    bool opt = ui->atc_SA_Manhattan->isChecked();
    ui->act_SA_EulerHeuristic->setChecked(!opt);

    if(opt){
        h = ManhattanHeuristic<int,int>();
    } else {
        h = EulerHeuristic<int,int>();
    }
}

void MainWindow::on_act_iterate_triggered()
{
    if(!mapLoaded)
        return;

    NodeGraph<int,int>* s = map->getStart();
    NodeGraph<int,int>* e = map->getEnd();

    if(s == NULL || e == NULL)
        return;

    ia->setStart(s);
    ia->setEnd(e);
    ia->iterate();
    emit updateMainCanvas();
}
