#ifndef FILEHELPER_H
#define FILEHELPER_H

#include <QString>
#include <vector>

using namespace std;

class FileHelper
{
public:
    static bool saveFile(vector<QString> text, QString fileName);
};

#endif // FILEHELPER_H
