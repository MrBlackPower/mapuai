#ifndef MANHATTANHEURISTIC_H
#define MANHATTANHEURISTIC_H

#include <heuristic.h>

template <class n_type, class e_type>

class ManhattanHeuristic : public Heuristic<n_type,e_type>
{
public:
    ManhattanHeuristic();

    double calculate(NodeGraph<n_type,e_type>* n1, NodeGraph<n_type,e_type>* n2);
};

#endif // MANHATTANHEURISTIC_H
