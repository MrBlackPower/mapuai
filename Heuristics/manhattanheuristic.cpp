#include "manhattanheuristic.h"

template <class n_type, class e_type>
ManhattanHeuristic<n_type,e_type>::ManhattanHeuristic() : Heuristic<n_type,e_type>()
{
    this->n = "MANHATTAN";
}

template <class n_type, class e_type>
double ManhattanHeuristic<n_type,e_type>::calculate(NodeGraph<n_type,e_type>* n1, NodeGraph<n_type,e_type>* n2){
    Point p1 = n1->getPoint();
    Point p2 = n2->getPoint();
    Point p = (p1 - p2);
    return (p.x + p.y + p.z);
}

template class ManhattanHeuristic<int,int>;
template class ManhattanHeuristic<float,int>;
template class ManhattanHeuristic<float,float>;
template class ManhattanHeuristic<double,int>;
template class ManhattanHeuristic<double,double>;
