#include "eulerheuristic.h"

template <class n_type, class e_type>
EulerHeuristic<n_type,e_type>::EulerHeuristic() : Heuristic<n_type,e_type>()
{
    this->n = "EULER";
}

template <class n_type, class e_type>
double EulerHeuristic<n_type,e_type>::calculate(NodeGraph<n_type,e_type>* n1, NodeGraph<n_type,e_type>* n2){
    Point p1 = n1->getPoint();
    Point p2 = n2->getPoint();

    return (p1 - p2).getMod();
}

template class EulerHeuristic<int,int>;
template class EulerHeuristic<float,int>;
template class EulerHeuristic<float,float>;
template class EulerHeuristic<double,int>;
template class EulerHeuristic<double,double>;
