#ifndef EULERHEURISTIC_H
#define EULERHEURISTIC_H

#include <heuristic.h>

template <class n_type, class e_type>

class EulerHeuristic : public Heuristic<n_type,e_type>
{
public:
    EulerHeuristic();
    double calculate(NodeGraph<n_type,e_type>* n1, NodeGraph<n_type,e_type>* n2);

};

#endif // EULERHEURISTIC_H
