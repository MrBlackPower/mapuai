#include "smartobject.h"

int SmartObject::N = 0;

SmartObject::SmartObject(){
    id = N;
    label = label.asprintf("[smrtObj='%d']",id);
    N++;
}

int SmartObject::getId(){
    return id;
}

QString SmartObject::getLabel(){
    return label;
}

void SmartObject::printOnConsole(){
    vector<QString> aux = print();

    for(int i = 0; i < aux.size(); i++){
        QString line = aux[i];
        cout << line.toLatin1().data() << endl;
    }
}

bool SmartObject::save(QString fileName){
    return FileHelper::saveFile(this->raw(),fileName);
}

vector<QString> SmartObject::print(){
    vector<QString> txt;
    QString line;
    line = "******************************************************************************";
    txt.push_back(line);
    line = "* SMART OBJECT                                                                      *";
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);
    line = label;
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);
    line = "* END OF SMART OBJECT                                                                      *";
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);
    return txt;
}

vector<QString> SmartObject::raw(){
    vector<QString> txt;
    QString line;
    line = "# mapUAI DataFile Version 1.0\n";
    txt.push_back(line);
    line = "# mapUAI output\n";
    txt.push_back(line);
    line = "# DATASET POLYDATA\n";
    txt.push_back(line);
    line = label;
    txt.push_back(line);

    return txt;
}

SmartObject::~SmartObject()
{
    N--;
}
