#ifndef POINT_H
#define POINT_H

#include <math.h>

class Point
{
    public:
        Point ();

        Point (int id, double x, double y);

        Point (int id, double x, double y, double z);

        ~Point ();

        int getID();

        double getMod();

        bool equals(Point* p);

        Point operator + (Point b);

        Point operator - (Point b);

        Point operator * (float f);

        double operator * (Point b);

        Point operator / (float f);

        bool operator == (Point b);

        void operator = (Point b);

        double x;
        double y;
        double z;

    protected:
        int id;
};

#endif // POINT_H
