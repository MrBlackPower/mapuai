#include "smartalgorithm.h"

template <class n_type, class e_type>
SmartAlgorithm<n_type,e_type>::SmartAlgorithm() : SmartObject(){
    hasMap = false;
    clock_wait = 0.0;
    strtd = false;
    fnshd = false;
    failed = false;
    iterations = 0;
    map = NULL;
    start = NULL;
    end = NULL;
    decisionTree = NULL;

    //DEFAULT HEURISTIC
    EulerHeuristic<n_type,e_type> aux;
    heuristic = (Heuristic<n_type,e_type>) aux;
}

template <class n_type, class e_type>
SmartAlgorithm<n_type,e_type>::~SmartAlgorithm(){
    if(hasTree)
        delete decisionTree;
}

template <class n_type, class e_type>
double SmartAlgorithm<n_type,e_type>::execute(){
    clock_t s = clock();

    if(!ready())
        return -1;

    while(!fnshd){
        iterate();
        usleep(clock_wait);
    }

    clock_t e = clock();
    return (double(e - s) / CLOCKS_PER_SEC);
}

template <class n_type, class e_type>
double SmartAlgorithm<n_type,e_type>::h(NodeGraph<n_type,e_type>* start, NodeGraph<n_type,e_type>* end){
    if(map == NULL)
        return -1;

    if(heuristic.name() == "NONE")
        return -1;
}

template <class n_type, class e_type>

bool SmartAlgorithm<n_type,e_type>::finished(){
    return fnshd;
}

template <class n_type, class e_type>
double SmartAlgorithm<n_type,e_type>::iterate(){
    clock_t s = clock();

    iteration();

    refresh();
    clock_t e = clock();
    return (double(e - s) / CLOCKS_PER_SEC);
}

template <class n_type, class e_type>
void SmartAlgorithm<n_type,e_type>::iteration(){

}

template <class n_type, class e_type>
vector<QString> SmartAlgorithm<n_type,e_type>::print(){
    return log;
}

template <class n_type, class e_type>
vector<QString> SmartAlgorithm<n_type,e_type>::raw(){
    return log;
}

template <class n_type, class e_type>
bool SmartAlgorithm<n_type,e_type>::ready(){
    if(map == NULL)
        return false;

    if(heuristic.name() == "NONE")
        return false;

    if(start == NULL)
        return false;

    if(end == NULL)
        return false;

    if(failed)
        return false;

    if(fnshd)
        return false;

    return true;
}

template <class n_type, class e_type>
void SmartAlgorithm<n_type,e_type>::refresh(){
    for(int i = 0; i < closed.size(); i++){
        NodeGraph<n_type,e_type>* n = closed[i];
        n->setStatus(CLOSED);
    }

    for(int i = 0; i < open.size(); i++){
        NodeGraph<n_type,e_type>* n = open[i];
        n->setStatus(OPEN);
    }
}

template <class n_type, class e_type>
bool SmartAlgorithm<n_type,e_type>::started(){
    return strtd;
}

template <class n_type, class e_type>
void SmartAlgorithm<n_type,e_type>::setClock(float c){
    if(c < 0.0)
        return;

    clock_wait = c;
}

template <class n_type, class e_type>
NodeGraph<n_type,e_type>* SmartAlgorithm<n_type,e_type>::getEnd(){
    return end;
}

template <class n_type, class e_type>
void SmartAlgorithm<n_type,e_type>::setEnd(NodeGraph<n_type,e_type>* e){
    if(map == NULL)
        return;

    if(!map->hasNode(e))
        return;

    end = e;
}

template <class n_type, class e_type>
Graph<n_type,e_type>* SmartAlgorithm<n_type,e_type>::getGraph(){
    return map;
}

template <class n_type, class e_type>
void SmartAlgorithm<n_type,e_type>::setGraph(Graph<n_type,e_type>* g){
    if(g == NULL){
        hasMap = false;
    }

    map = g;
}

template <class n_type, class e_type>
NodeGraph<n_type,e_type>* SmartAlgorithm<n_type,e_type>::getStart(){
    return start;
}

template <class n_type, class e_type>
void SmartAlgorithm<n_type,e_type>::setStart(NodeGraph<n_type,e_type>* s){
    if(map == NULL)
        return;

    if(!map->hasNode(s))
        return;

    start = s;
}

template <class n_type, class e_type>
bool SmartAlgorithm<n_type,e_type>::hasDecisionTree(){
    return hasTree;
}

template <class n_type, class e_type>
Tree<int>* SmartAlgorithm<n_type,e_type>::getTree(){
    return decisionTree;
}

template <class n_type, class e_type>
void SmartAlgorithm<n_type,e_type>::setTree(Tree<int>* t){
    decisionTree = t;
}

template class SmartAlgorithm<int,int>;
template class SmartAlgorithm<float,int>;
template class SmartAlgorithm<float,float>;
template class SmartAlgorithm<double,int>;
template class SmartAlgorithm<double,double>;
