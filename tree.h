#ifndef TREE_H
#define TREE_H

#define SPHERE_RADIUS 10.0

#include <vector>
#include <QString>
#include <nodetree.h>
#include <graphicobject.h>

using namespace std;

template <class n_type>

class Tree : public GraphicObject
{
public:
    Tree();
    ~Tree();

    bool addNode(QString label, n_type data);
    bool addNode(QString label, double x, double y, n_type data);
    bool deleteNode(double x, double y);
    bool deleteNode(NodeTree<n_type>* n);
    bool deleteNode();
    bool hasNode(NodeTree<n_type>* n);
    NodeTree<n_type>* selectNode(double x, double y, double radius);
    vector<QString> print();
    vector<QString> raw();
    void draw(QOpenGLWidget* widget);
    void mousePress2D(double x, double y, QMouseEvent* e);
    void mouseMove2D(double x, double y, QMouseEvent* e);

    void goToRoot();
    void goToFather();
    void goToNSon(int n);

    vector<NodeTree<n_type>*> getIteratorSons();
    NodeTree<n_type>* getIterator();
private:
    vector<QString> print(NodeTree<n_type>* n);

    int N; // number of nodes
    int M; // number of edges
    NodeTree<n_type>* root;
    vector<NodeTree<n_type>*> nodes;
    NodeTree<n_type>* iterator;
};

#endif // TREE_H
