#ifndef NODETREE_H
#define NODETREE_H

#include "node.h"
#include <vector>
#include <QString>

using namespace std;

template <class n_type>

class NodeTree : public Node<n_type>
{
public:
    NodeTree(int id, QString label, double x, double y, n_type data);

    void addSon(NodeTree* n);
    void removeSon(NodeTree* n);
    vector<NodeTree*> getSons();

    NodeTree* getFather();
    void setFather(NodeTree* father);

    ~NodeTree();

private:

    NodeTree* father;
    vector<NodeTree*> sons;
    int S; //NUMBER OF SONS
};

#endif // NODETREE_H
