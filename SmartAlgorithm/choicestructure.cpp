#include "choicestructure.h"

template <class e_type>
ChoiceStructure<e_type>::ChoiceStructure()
{
    this->father_id = 0;
    this->node_id = 0;
    this->cost_estimated = 0;
    this->cost_heuristic = 0;
}

template <class e_type>
ChoiceStructure<e_type>::ChoiceStructure(int father_id, int node_id, e_type node_cost, double cost_heuristic, double cost_estimated)
{
    this->father_id = father_id;
    this->node_id = node_id;
    this->node_cost = node_cost;
    this->cost_estimated = cost_estimated;
    this->cost_heuristic = cost_heuristic;
}

template class ChoiceStructure<int>;
template class ChoiceStructure<float>;
template class ChoiceStructure<double>;
