#ifndef DEPTH_H
#define DEPTH_H

#include "../smartalgorithm.h"

using namespace std;

template <class n_type, class e_type>
class Depth : public SmartAlgorithm<n_type,e_type>{
    using SmartAlgorithm<n_type,e_type>::open;
    using SmartAlgorithm<n_type,e_type>::strtd;
    using SmartAlgorithm<n_type,e_type>::failed;
    using SmartAlgorithm<n_type,e_type>::fnshd;
    using SmartAlgorithm<n_type,e_type>::open_choices;
    using SmartAlgorithm<n_type,e_type>::closed;
    using SmartAlgorithm<n_type,e_type>::ready;
    using SmartAlgorithm<n_type,e_type>::start;
    using SmartAlgorithm<n_type,e_type>::end;
    using SmartAlgorithm<n_type,e_type>::hasTree;
    using SmartAlgorithm<n_type,e_type>::decisionTree;
    using SmartAlgorithm<n_type,e_type>::map;
    using SmartAlgorithm<n_type,e_type>::h;
    using SmartAlgorithm<n_type,e_type>::iterations;
public:
    Depth();
    ~Depth();

    void iteration();
};

#endif // DEPTH_H
