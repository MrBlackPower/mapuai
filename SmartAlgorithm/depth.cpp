#include "depth.h"

template <class n_type, class e_type>
Depth<n_type,e_type>::Depth() : SmartAlgorithm<n_type,e_type>()
{

}

template <class n_type, class e_type>
void Depth<n_type,e_type>::iteration()
{
    if(!ready())
        return;


    cout << " >> ITERATION #" << iterations << " << " << endl;

    if(open.size() == 0){
        if(!strtd){
            ChoiceStructure<int> choice = ChoiceStructure<int>(-1,start->getId(),0,h(start,end),0);
            //STARTS OPEN LIST
            open.push_back(start);
            open_choices.push_back(choice);

            //CREATES DECISION TREE
            decisionTree = new Tree<int>();
            hasTree = true;
            decisionTree->addNode("ROOT",choice.node_id);
            strtd = true;
        } else {
            cout << " >> ALGORITHM FAILED << " << endl;
            failed = true;
            fnshd = true;
            strtd = false;
        }
    } else {
        if(strtd){
            //CHOOSES LAST IN CHOICES VECTOR
            ChoiceStructure<int> choice = open_choices[(open_choices.size() - 1)];
            cout << " >> HAS CHOSEN THE #" <<choice.node_id << " NODE AS THE NEXT STEP << " << endl;
            open_choices.pop_back();

            //WALKS DECISION TREE TO THE DESIRED NODE
            if(choice.node_id != start->getId()){
                vector<NodeTree<int>*> sons = decisionTree->getIteratorSons();

                bool found = false;
                for(int i = 0; i < sons.size() && !found; i++){
                    NodeTree<int>* aux = sons[i];
                    if(aux->data == choice.node_id){
                        decisionTree->goToNSon(i);
                        found = true;
                    }
                }

                if(!found){
                    cout << " >> ALGORITHM FAILED << " << endl;
                    failed = true;
                    fnshd = true;
                    strtd = false;
                }
            }

            //SELECTS THE CURRENT NODE
            NodeGraph<n_type,e_type>* n = map->operator [](choice.node_id);
            closed.push_back(n);

            //CHECKS IF ITS THE END NODE
            if(end->equals(n)){
                cout << " >> ALGORITHM FINISHED WITH COST:"<< choice.node_cost <<" << " << endl;
                failed = false;
                fnshd = true;
                strtd = false;
            }

            //OPEN CHOICES IT OFFERS
            vector<NodeGraph<n_type,e_type>*> options = n->getEdges();
            for(int i = options.size() - 1; i >= 0; i --){
                NodeGraph<n_type,e_type>* nAux = options[i];
                ChoiceStructure<int> choiceAux = ChoiceStructure<int>(choice.node_id,nAux->getId(),choice.node_cost + 1,0,0);

                //CHECKS IF NODE IS NOT CLOSED
                bool found = false;
                for(int j = 0; j < closed.size() && !found; j++)
                    if(nAux->equals(closed[j]))
                        found = true;

                //IF NOT CLOSED ADDS NODE TO THE LIST
                if(!found){
                    open.push_back(nAux);
                    open_choices.push_back(choiceAux);
                    QString l;
                    decisionTree->addNode(l.asprintf("%d#%d",choice.node_id,nAux->getId()),choice.node_cost);
                }
            }

            //CLOSES THIS NODE
            //IF NODE IS OPEN REMOVED FROM OPEN LIST
            for(int j = 0; j < open.size(); j++){
                if(n->equals(open[j])){
                    open.erase(open.begin() + j);
                    j--;
                }
            }

        } else {
            cout << " >> ALGORITHM RESTARTED << " << endl;
            failed = false;
            fnshd = false;
            strtd = false;
        }
    }
    iterations++;
}

template <class n_type, class e_type>
Depth<n_type,e_type>::~Depth()
{

}

template class Depth<int,int>;
template class Depth<float,int>;
template class Depth<float,float>;
template class Depth<double,int>;
template class Depth<double,double>;
