#ifndef CHOICESTRUCTURE_H
#define CHOICESTRUCTURE_H

#include <QString>

template <class e_type>
class ChoiceStructure{
public:
    ChoiceStructure(int father_id, int node_id, e_type node_cost, double cost_heuristic, double cost_estimated);
    ChoiceStructure();

    int father_id;
    int node_id;
    e_type node_cost;
    double cost_heuristic;
    double cost_estimated;
};

#endif // CHOICESTRUCTURE_H
