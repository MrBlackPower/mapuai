#ifndef NODEGRAPH_H
#define NODEGRAPH_H

#include "node.h"
#include <vector>
#include <QString>

using namespace std;

template <class n_type, class e_type>

class NodeGraph : public Node<n_type>
{
    public:
        NodeGraph(int id, QString label, double x, double y, n_type data);

        void addEdge(NodeGraph* n, e_type cost);
        void removeEdge(NodeGraph* n);
        vector<NodeGraph<n_type,e_type>*> getEdges();
        vector<e_type> getCosts();

        ~NodeGraph();

    private:
        vector<NodeGraph*> edges;
        vector<e_type> cost;
        int E; //NUMBER OF EDGES
};

#endif // NODEGRAPH_H
