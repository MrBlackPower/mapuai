#include "tree.h"

template <class n_type>
Tree<n_type>::Tree()
{

}

template <class n_type>
Tree<n_type>::~Tree()
{

}

template <class n_type>
bool Tree<n_type>::addNode(QString label, n_type data){
    addNode(label,0.0,0.0,data);
}

template <class n_type>
bool Tree<n_type>::addNode(QString label, double x, double y, n_type data){
    NodeTree<n_type>* n = new NodeTree<n_type>(N,label,x,y,data);

    if(N == 0){
        root = n;
        iterator = root;
        setGraphicSize(Point(ZERO,x,y));
    } else {
        if(iterator == NULL || root == NULL)
            return false;

        iterator->addSon(n);
        adjustBorder(Point(ZERO,x,y));
    }

    nodes.push_back(n);
    N++;

    return true;
}

template <class n_type>
bool Tree<n_type>::deleteNode(double x, double y){
    return deleteNode(selectNode(x,y,0.0));
}

template <class n_type>
bool Tree<n_type>::deleteNode(){
    return deleteNode(iterator);
}

template <class n_type>
bool Tree<n_type>::deleteNode(NodeTree<n_type>* n){
    //Deletes Node from Nodes List
    for(int i = 0; i < nodes.size(); i++){
        if(n->equals(nodes[i])){
            if(iterator->equals(n))
                if(n->getFather() != NULL)
                    iterator = n->getFather();

            //Deletes all Nodes below this node.
            vector<NodeTree<n_type>*> sons = n->getSons();

            for(int i = 0; i < sons.size(); i++)
                deleteNode(sons[i]);

            //Deletes all Edges that references n
            for(int j = 0; j < nodes.size(); j++){
                nodes[j]->removeSon(nodes[i]);
            }

            nodes.erase(nodes.begin() + i);

            //ADJUSTS SIZE OF THE GRAPHIC OBJECT
            if(N == 0){
                setGraphicSize(Point(ZERO,ZERO,ZERO));
            } else {
                vector<Point> aux;

                for(int j = 0; j < nodes.size(); j++){
                    NodeTree<n_type>* n = nodes[i];
                    aux.push_back(n->getPoint());
                }

                setGraphicSize(aux);
            }

            N--;
            delete n;
            return true;
        }
    }

    return false;
}

template <class n_type>
NodeTree<n_type>* Tree<n_type>::selectNode(double x, double y, double radius){
    Point p = Point(0,x,y);

    for(int i = 0; i < nodes.size(); i++){
        NodeTree<n_type>* n = nodes[i];
        Point aux = p - n->getPoint();
        if(aux.getMod() <= radius)
            return n;
    }

    return NULL;
}

template <class n_type>
vector<QString> Tree<n_type>::print(){
    vector<QString> txt;
    QString line;
    line = "******************************************************************************";
    txt.push_back(line);
    line = "* TREE                                                                      *";
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);
    line = line.asprintf("N= %d - M = %d", N, M);
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);

    if(root != NULL){
        print(root);
    }

    line = "******************************************************************************";
    txt.push_back(line);
    line = "* END OF GRAPH                                                               *";
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);

    return txt;
}

template <class n_type>
vector<QString> Tree<n_type>::print(NodeTree<n_type>* n){
    vector<QString> txt;
    QString line;
    line = "******************************************************************************";
    txt.push_back(line);
    line = line.asprintf("* NODE (%s)                                                                  *"
                         , n->getLabel().toStdString());
    txt.push_back(line);
    line = "********************************SONS*****************************************";
    txt.push_back(line);

    vector<NodeTree<n_type>*> sons = n->getSons();
    for(int j = 0; j < sons.size(); j++){
        line = line.asprintf("( %d ) %s -> %s",j,n->getLabel().toStdString(),sons[j]->getLabel().toStdString());
        txt.push_back(line);
    }

    line = "******************************************************************************";
    txt.push_back(line);

    for(int k = 0; k < sons.size(); k++){
        NodeTree<n_type>* aux = sons[k];
        vector<QString> txtAux = print(aux);

        //Adds recursion Text
        for(int l = 0; l < txtAux.size(); l++)
            txt.push_back(txtAux[l]);
    }

    return txt;
}

template <class n_type>
vector<QString> Tree<n_type>::raw(){
    vector<QString> txt;
    QString line;
    line = "# mapUAI DataFile Version 1.0\n";
    txt.push_back(line);
    line = "# mapUAI output\n";
    txt.push_back(line);
    line = "# DATASET POLYDATA\n";
    txt.push_back(line);
    line = line.asprintf("POINTS  %d  double\n", N );
    txt.push_back(line);

    //PRINTS POINTS
    for(int i = 0; i < nodes.size(); i++){
        Point p = nodes[i]->getPoint();
        line = line.asprintf("%f  %f  %f\n", p.x, p.y, p.z);
        txt.push_back(line);
    }

    //PRINTS LINES
    for(int i = 0; i < nodes.size(); i++){
        vector<NodeTree<n_type>*> nAux = nodes[i]->getSons();
        for(int j = 0; j < nAux.size(); j++){
            line = line.asprintf("%d  %d\n", i, j);
            txt.push_back(line);
        }
    }

    txt.push_back(line);
    return txt;
}

template <class n_type>
void Tree<n_type>::draw(QOpenGLWidget* widget){
    widget->height();
    for(int i = 0; i < nodes.size(); i++){
        NodeTree<n_type>* n = nodes[i];
        vector<NodeTree<n_type>*> nSons = n->getSons();
        Point start = n->getPoint();

        //DRAWS LINES
        for(int j = 0; j < nSons.size(); j++){
            NodeTree<n_type>* s = nSons[j];
            Point end = s->getPoint();
            drawLine(widget,start,end);
        }

        //DRAWS NODE
        drawSphere(widget,SPHERE_RADIUS,start);
    }
}

template <class n_type>
void Tree<n_type>::mouseMove2D(double x, double y, QMouseEvent* e){

}

template <class n_type>
void Tree<n_type>::mousePress2D(double x, double y, QMouseEvent* e){

}

template <class n_type>
bool Tree<n_type>::hasNode(NodeTree<n_type>* n){
    for(int i = 0; i < nodes.size(); i ++)
        if(n->equals(nodes[i]))
            return true;

    return false;
}

template <class n_type>
void Tree<n_type>::goToRoot(){
    iterator = root;
}

template <class n_type>
void Tree<n_type>::goToFather(){
    if(iterator->getFather() != NULL)
        iterator = iterator->getFather();
}

template <class n_type>
void Tree<n_type>::goToNSon(int n){
    vector<NodeTree<n_type>*> sons = iterator->getSons();
    if(n < sons.size())
        if(sons[n] != NULL)
            iterator = sons[n];
}

template <class n_type>
vector<NodeTree<n_type>*> Tree<n_type>::getIteratorSons(){
    vector<NodeTree<n_type>*> sons = iterator->getSons();

    return sons;
}

template <class n_type>
NodeTree<n_type>* Tree<n_type>::getIterator(){
    return iterator;
}

template class Tree<int>;
template class Tree<float>;
template class Tree<double>;

