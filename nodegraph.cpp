#include "nodegraph.h"

#include "node.h"

template <class n_type, class e_type>
NodeGraph<n_type,e_type>::NodeGraph(int id, QString label, double x, double y, n_type data) : Node<n_type>(id, label, x, y, data) {
    E = 0;
}

template <class n_type, class e_type>
void NodeGraph<n_type,e_type>::addEdge(NodeGraph* n, e_type cost){
    edges.push_back(n);
    this->cost.push_back(cost);
    E++;
}

template <class n_type, class e_type>
void NodeGraph<n_type,e_type>::removeEdge(NodeGraph* n){
    for(int i = 0; i < edges.size(); i++){
        if(n->equals(edges[i])){
            NodeGraph* nAux = edges[i];
            edges[i] = edges[edges.size() - 1];
            edges[edges.size() - 1] = nAux;
            edges.pop_back();
            int cAux = cost[i];
            cost[i] = cost[cost.size() - 1];
            cost[cost.size() - 1] = cAux;
            cost.pop_back();

            E--;
            return;
        }
    }
}

template <class n_type, class e_type>
vector<NodeGraph<n_type,e_type>*> NodeGraph<n_type,e_type>::getEdges(){
    vector<NodeGraph<n_type,e_type>*> aux = edges;
    return aux;
}

template <class n_type, class e_type>
vector<e_type> NodeGraph<n_type,e_type>::getCosts(){
    vector<e_type> aux = cost;
    return aux;
}

template <class n_type, class e_type>
NodeGraph<n_type,e_type>::~NodeGraph(){

}

template class NodeGraph<int,int>;
template class NodeGraph<float,int>;
template class NodeGraph<float,float>;
template class NodeGraph<double,int>;
template class NodeGraph<double,double>;
