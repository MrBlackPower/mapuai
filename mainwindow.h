#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <smartobject.h>
#include <graph.h>
#include <tree.h>
#include <heuristic.h>
#include <Heuristics/eulerheuristic.h>
#include <Heuristics/manhattanheuristic.h>
#include <smartalgorithm.h>
#include <SmartAlgorithm/depth.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_act_graph_new_triggered();

    void on_act_graph_purge_triggered();

    void on_act_populate_random_triggered();

    void on_act_square_case_triggered();

    void on_act_SA_EulerHeuristic_triggered();

    void on_atc_SA_Manhattan_triggered();

    void on_act_iterate_triggered();

signals:
    void updateMainCanvas();

    void updateRightCanvas();

private:
    Ui::MainWindow *ui;

    bool mapLoaded;
    bool mapIterated;
    Graph<int,int>* map;
    Tree<int>* decisionTree;
    Heuristic<int,int> h;
    SmartAlgorithm<int,int>* ia;
};

#endif // MAINWINDOW_H
