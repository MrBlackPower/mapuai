#include "graph.h"

template <class n_type, class e_type>
Graph<n_type,e_type>::Graph() : GraphicObject(){
    N = 0;
    M = 0;
    start = NULL;
    end = NULL;
    selected = NULL;
    selectedStatus = IDLE;
}

template <class n_type, class e_type>
Graph<n_type,e_type>::~Graph(){

}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::addNode(double x, double y, n_type data){
    QString label = label.asprintf("NODE_GRAPH_%d",N);

    NodeGraph<n_type,e_type>* n = new NodeGraph<n_type,e_type>(N,label,x,y,data);

    //ADJUSTS SIZE OF GRAPHIC OBJECT
    if(N == 0){
        setGraphicSize(Point(ZERO,x,y));
    } else {
        adjustBorder(Point(ZERO,x,y));
    }

    nodes.push_back(n);
    N++;

    return true;
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::deleteNode(double x, double y){
    return deleteNode(selectNode(x,y,0.0));
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::deleteNode(NodeGraph<n_type,e_type>* n){
    //Deletes Node from Nodes List
    for(int i = 0; i < nodes.size(); i++){
        if(n->equals(nodes[i])){
            //Deletes all Edges that references n
            for(int j = 0; j < nodes.size(); j++){
                nodes[j]->removeEdge(nodes[i]);
            }

            nodes.erase(nodes.begin() + i);

            //ADJUSTS SIZE OF THE GRAPHIC OBJECT
            if(N == 0){
                setGraphicSize(Point(ZERO,ZERO,ZERO));
            } else {
                vector<Point> aux;

                for(int j = 0; j < nodes.size(); j++){
                    NodeGraph<n_type,e_type>* n = nodes[i];
                    aux.push_back(n->getPoint());
                }

                setGraphicSize(aux);
            }

            N--;
            delete n;
            return true;
        }
    }

    return false;
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::hasNode(NodeGraph<n_type,e_type>* n){
    for(int i = 0; i < nodes.size(); i ++)
        if(n->equals(nodes[i]))
            return true;

    return false;
}

template <class n_type, class e_type>
void Graph<n_type,e_type>::populate(){
    populate((int) MathHelper::random(RANDOM_RADIUS,ZERO));
}

template <class n_type, class e_type>
void Graph<n_type,e_type>::populate(int N){
    for(int i = 0; i < N; i ++){
        double x = MathHelper::random(RANDOM_RADIUS,ZERO);
        double y = MathHelper::random(RANDOM_RADIUS,ZERO);
        addNode(x,y,NULL);
    }
}

template <class n_type, class e_type>
void Graph<n_type,e_type>::populate(vector<n_type> data){
    for(int i = 0; i < data.size(); i ++){
        double x = MathHelper::random(RANDOM_RADIUS,ZERO);
        double y = MathHelper::random(RANDOM_RADIUS,ZERO);
        addNode(x,y,data[i]);
    }
}

template <class n_type, class e_type>
void Graph<n_type,e_type>::populate(vector<Point> pos, vector<n_type> data){
    if(pos.size() != data.size())
        return;

    for(int i = 0; i < data.size(); i ++){
        Point p = pos[i];
        addNode(p.x,p.y,data[i]);
    }
}

template <class n_type, class e_type>
int Graph<n_type,e_type>::size(){
    return N;
}

template <class n_type, class e_type>
NodeGraph<n_type,e_type>* Graph<n_type,e_type>::operator[] (int n){
    if(n < N)
        return nodes[n];

    return NULL;
}

template <class n_type, class e_type>
NodeGraph<n_type,e_type>* Graph<n_type,e_type>::operator() (int n,int e){
    if(n < N){
        NodeGraph<n_type,e_type>* node = nodes[n];
        vector<NodeGraph<n_type,e_type>*> edges = node->getEdges();
        if(e < edges.size()){
            return edges[e];
        }
    }

    return NULL;
}

template <class n_type, class e_type>
NodeGraph<n_type,e_type>* Graph<n_type,e_type>::selectNode(double x, double y, double radius){
    Point p = Point(0,x,y);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<n_type,e_type>* n = nodes[i];
        Point aux = p - n->getPoint();
        if(aux.getMod() <= radius)
            return n;
    }

    return NULL;
}

template <class n_type, class e_type>
NodeGraph<n_type,e_type>* Graph<n_type,e_type>::getStart(){
    return start;
}

template <class n_type, class e_type>
NodeGraph<n_type,e_type>* Graph<n_type,e_type>::getEnd(){
    return end;
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::addEdge(bool directional, NodeGraph<n_type,e_type>* start, NodeGraph<n_type,e_type>* end, e_type weight){
    int nFound = 0; //counter of nodes found
    int startID = 0;
    int endID = 0;
    for(int i = 0; i < nodes.size() || nFound < 2; i++){
        if(start->equals(nodes[i])){
            startID = i;
            nFound ++;
        }

        if(end->equals(nodes[i])){
            endID = i;
            nFound++;
        }
    }

    if(nFound != 2)
        return false;

    if(directional){
        nodes[startID]->addEdge(nodes[endID],weight);
    } else {
        nodes[startID]->addEdge(nodes[endID],weight);
        nodes[endID]->addEdge(nodes[startID],weight);
    }

    return true;
}

template <class n_type, class e_type>
vector<QString> Graph<n_type,e_type>::print(){
    vector<QString> txt;
    QString line;
    line = "******************************************************************************";
    txt.push_back(line);
    line = "* GRAPH                                                                      *";
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);
    line = line.asprintf("N= %d - M = %d", N, M);
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<n_type,e_type>* currentNode = nodes[i];
        line = "******************************************************************************";
        txt.push_back(line);
        line = line.asprintf("* NODE (%s)                                                                  *"
                             , currentNode->getLabel().toLatin1().data());
        txt.push_back(line);
        line = line.asprintf(" * AT (%.3f,.3f)                                                 *",currentNode->getPoint().x,currentNode->getPoint().y);
        line = "********************************EDGES*****************************************";
        txt.push_back(line);

        vector<NodeGraph<n_type,e_type>*> edges = currentNode->getEdges();
        vector<e_type> costs = currentNode->getCosts();
        int start = currentNode->getId();
        QString lblStart = line.asprintf("(%.3f,%.3f,%.3f)",currentNode->getPoint().x,currentNode->getPoint().y,currentNode->getPoint().z);

        for(int j = 0; j < edges.size(); j++){
            int end = edges[j]->getId();
            QString lblEnd = line.asprintf("(%.3f,%.3f,%.3f)",edges[j]->getPoint().x,edges[j]->getPoint().y,edges[j]->getPoint().z);
            e_type cost = costs[j];
            line = line.asprintf("( %d ) %s#%d -> %s#%d, %d",j,lblStart.toLatin1().data(),start,lblEnd.toLatin1().data(),end,cost);
            txt.push_back(line);
        }

        line = "******************************************************************************";
        txt.push_back(line);
    }

    line = "******************************************************************************";
    txt.push_back(line);
    line = "* END OF GRAPH                                                               *";
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);

    return txt;
}

template <class n_type, class e_type>
vector<QString> Graph<n_type,e_type>::raw(){
    vector<QString> txt;
    QString line;
    line = "# mapUAI DataFile Version 1.0\n";
    txt.push_back(line);
    line = "# mapUAI output\n";
    txt.push_back(line);
    line = "# DATASET POLYDATA\n";
    txt.push_back(line);
    line = line.asprintf("POINTS  %d  double\n", N );
    txt.push_back(line);

    //PRINTS POINTS
    for(int i = 0; i < nodes.size(); i++){
        Point p = nodes[i]->getPoint();
        line = line.asprintf("%f  %f  %f\n", p.x, p.y, p.z);
        txt.push_back(line);
    }

    //PRINTS LINES (FOLLOWED BY WEIGHT)
    for(int i = 0; i < nodes.size(); i++){
        vector<NodeGraph<n_type,e_type>*> nAux = nodes[i]->getEdges();
        vector<e_type> cAux = nodes[i]->getCosts();
        for(int j = 0; j < nAux.size(); j++){
            line = line.asprintf("%d  %d %d\n", i, j,cAux[j]);
            txt.push_back(line);
        }
    }

    txt.push_back(line);
    return txt;
}

template <class n_type, class e_type>
int Graph<n_type,e_type>::edges(){
    return M;
}

template <class n_type, class e_type>
void Graph<n_type,e_type>::draw(QOpenGLWidget* widget){
    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<n_type,e_type>* n = nodes[i];
        vector<NodeGraph<n_type,e_type>*> nEdges = n->getEdges();
        Point start = n->getPoint();

        //DRAWS LINES
        for(int j = 0; j < nEdges.size(); j++){
            NodeGraph<n_type,e_type>* s = nEdges[j];
            Point end = s->getPoint();
            drawLine(widget,start,end);
        }
    }
    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<n_type,e_type>* n = nodes[i];
        vector<NodeGraph<n_type,e_type>*> nEdges = n->getEdges();
        Point start = n->getPoint();

        //DRAWS NODE
        drawSphere(widget,SPHERE_RADIUS,start,n->getColor());
    }
}

template <class n_type, class e_type>
void Graph<n_type,e_type>::mouseMove2D(double x, double y, QMouseEvent* e){

    if(selected != NULL)
        selected->setStatus(selectedStatus);

    if (e->buttons() & Qt::LeftButton) {
        return;
    } else if (e->buttons() & Qt::RightButton) {
        return;
    } else {
        NodeGraph<n_type,e_type>* s = selectNode(x,y,((getWidth())*0.02));

        if(s == NULL)
            return;

        selectedStatus = s->getStatus();
        selected = s;
        cout << "SELECTED >> " << s->getLabel().toLatin1().data() << endl;
        s->setStatus(SELECTED);
    }
}

template <class n_type, class e_type>
void Graph<n_type,e_type>::mousePress2D(double x, double y, QMouseEvent* e){
    cout << ">> MOUSE PRESS <<" << endl;
    if (e->buttons() & Qt::LeftButton) {
        if(start != NULL)
            start->setStatus(IDLE);

        if(selected == NULL)
            return;

        cout << "START SELECTED >> " << selected->getLabel().toLatin1().data() << endl;
        selected->setStatus(START);
        selectedStatus = START;
        start = selected;
        return;
    } else if (e->buttons() & Qt::RightButton) {
        if(end != NULL)
            end->setStatus(IDLE);

        if(selected == NULL)
            return;

        cout << "END SELECTED >> " << selected->getLabel().toLatin1().data() << endl;
        selected->setStatus(END);
        selectedStatus = END;
        end = selected;
        return;
    } else {

    }


}

template class Graph<int,int>;
template class Graph<float,int>;
template class Graph<float,float>;
template class Graph<double,int>;
template class Graph<double,double>;
