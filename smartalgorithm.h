#ifndef SMARTALGORITHM_H
#define SMARTALGORITHM_H

#include "SmartAlgorithm/choicestructure.h"
#include <smartobject.h>
#include <graph.h>
#include <tree.h>
#include <ctime>
#include <unistd.h>
#include <heuristic.h>
#include <Heuristics/eulerheuristic.h>

template <class n_type, class e_type>
class SmartAlgorithm : public SmartObject
{
public:
    SmartAlgorithm();
    ~SmartAlgorithm();

    double execute();
    bool finished();
    double h(NodeGraph<n_type,e_type>* start, NodeGraph<n_type,e_type>* end);
    double iterate();
    vector<QString> print();
    vector<QString> raw();
    bool ready();
    bool started();

    void setClock(float c);

    NodeGraph<n_type,e_type>* getEnd();
    void setEnd(NodeGraph<n_type,e_type>* e);

    Graph<n_type,e_type>* getGraph();
    void setGraph(Graph<n_type,e_type>* g);

    NodeGraph<n_type,e_type>* getStart();
    void setStart(NodeGraph<n_type,e_type>* s);

    bool hasDecisionTree();
    Tree<int>* getTree();
    void setTree(Tree<int>* t);

    virtual void iteration();

protected:
    void refresh();

    float clock_wait;
    int iterations;
    bool strtd;
    bool hasMap;
    bool failed;
    bool fnshd;
    Graph<n_type,e_type>* map;

    //USED IN THE PROBLEM
    NodeGraph<n_type,e_type>* start;
    NodeGraph<n_type,e_type>* end;
    vector<NodeGraph<n_type,e_type>*> open;
    vector<NodeGraph<n_type,e_type>*> closed;
    bool hasTree;
    Tree<int>* decisionTree;
    Heuristic<n_type,e_type> heuristic;
    vector<QString> log;
    vector<ChoiceStructure<int>> open_choices;

private:

};

#endif // SMARTALGORITHM_H
