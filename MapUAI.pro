#-------------------------------------------------
#
# Project created by QtCreator 2018-04-26T17:17:31
#
#-------------------------------------------------
DEPENDPATH += \
                /usr/include/GL
INCLUDEPATH += \
               /usr/include/GL

greaterThan(QT_MAJOR_VERSION, 4):
QT += widgets
QT += core gui
QT += opengl
LIBS += -lglut -lGLU

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MapUAI
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    nodegraph.cpp \
    point.cpp \
    mathhelper.cpp \
    filehelper.cpp \
    tree.cpp \
    node.cpp \
    nodetree.cpp \
    graphicobject.cpp \
    smartobject.cpp \
    oglcanvas.cpp \
    graph.cpp \
    heuristic.cpp \
    smartalgorithm.cpp \
    Heuristics/manhattanheuristic.cpp \
    Heuristics/eulerheuristic.cpp \
    SmartAlgorithm/choicestructure.cpp \
    SmartAlgorithm/depth.cpp

HEADERS += \
        mainwindow.h \
    nodegraph.h \
    point.h \
    mathhelper.h \
    filehelper.h \
    tree.h \
    node.h \
    nodetree.h \
    graphicobject.h \
    smartobject.h \
    oglcanvas.h\
    graph.h \
    heuristic.h \
    smartalgorithm.h \
    Heuristics/manhattanheuristic.h \
    Heuristics/eulerheuristic.h \
    SmartAlgorithm/choicestructure.h \
    SmartAlgorithm/depth.h

FORMS += \
        mainwindow.ui
