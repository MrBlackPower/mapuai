#ifndef MATHHELPER_H
#define MATHHELPER_H

#include <vector>
#include <math.h>
#include <cstdlib>

using namespace std;

class MathHelper
{
public:
    /************************************************************************************
            Finds the biggest value in the vector.

            @return maximum value.
    ************************************************************************************/
            static float max(vector<float> v);


    /************************************************************************************
            Finds the smaller value in the vector.

            @return maximum value.
    ************************************************************************************/
            static float min(vector<float> v);

            static double random(double max, double min);
};

#endif // MATHHELPER_H
