#include "mathhelper.h"

/************************************************************************************
        Finds the biggest value in the vector.

        @return maximum value.
************************************************************************************/
float MathHelper::max(vector<float> v){
    if(v.size() == 0)
        return -1;

    float max = v[0];

    for(int i = 1; i < v.size(); i++){
        if(v[i] > max)
            max = v[i];
    }

    return max;

}

double MathHelper::random(double max, double min){
    if(min > max){
        double aux = max;
        max = min;
        min = aux;
    }

    return min + (rand() % ((int)max - (int)min));
}


/************************************************************************************
        Finds the smaller value in the vector.

        @return maximum value.
************************************************************************************/
float MathHelper::min(vector<float> v){
    if(v.size() == 0)
        return -1;

    float min = v[0];

    for(int i = 1; i < v.size(); i++){
        if(v[i] < min)
            min = v[i];
    }

    return min;

}
