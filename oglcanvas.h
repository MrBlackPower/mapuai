#ifndef OGLCANVAS_H
#define OGLCANVAS_H

#include <QtOpenGL/QGL>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QMouseEvent>
#include <mathhelper.h>
#include <graphicobject.h>
#include <point.h>

using namespace std;

class OglCanvas : public QOpenGLWidget
{
public:
        OglCanvas(QWidget *parent = 0);

        void purgeGraphicObect();
        void setGraphicObject(GraphicObject* graphicObject);

        ~OglCanvas();

signals:

        static void mousePress(QMouseEvent*);
        static void mouseMove(QMouseEvent*);

protected:
    void initializeGL();

    void resizeGL(int w, int h);

    void paintGL();

    void mousePressEvent(QMouseEvent* event);

    void mouseMoveEvent(QMouseEvent* event);

public slots:
    void repaint();

private:
    bool loaded;

    float scale;

    Point* transform;

    GraphicObject* graphicObject;

    QPoint lastPos;

    int height;

    int width;
};

#endif // OGLMAINCANVAS_H
