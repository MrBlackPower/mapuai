#ifndef SMARTOBJECT_H
#define SMARTOBJECT_H

#include <QString>
#include <filehelper.h>
#include <iostream>

using namespace std;

class SmartObject
{
public:
    static int N;
    SmartObject();
    ~SmartObject();

    int getId();
    QString getLabel();
    void printOnConsole();
    bool save(QString fileName);

    virtual vector<QString> print();
    virtual vector<QString> raw();


private:
    int id;
    QString label;
};


#endif // SMARTOBJECT_H
