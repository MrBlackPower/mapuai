#ifndef HEURISTIC_H
#define HEURISTIC_H

#include <nodegraph.h>
#include <point.h>

template <class n_type, class e_type>

class Heuristic
{
public:
    Heuristic();

    QString name();

    virtual double calculate(NodeGraph<n_type,e_type>* n1, NodeGraph<n_type,e_type>* n2);

protected:
    QString n;

    void setName(QString name);
};

#endif // HEURISTIC_H
