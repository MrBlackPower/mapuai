#ifndef GRAPHICOBJECT_H
#define GRAPHICOBJECT_H

#define ZERO 0

#define LEFT      100
#define RIGHT     101
#define BOTTOM    102
#define TOP       103
#define NEAR      104
#define FAR       105

#define START     200
#define END       201
#define OPEN      202
#define CLOSED    203
#define IDLE      204
#define SELECTED  205

#include <smartobject.h>
#include <point.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <QFont>
#include <QOpenGLWidget>
#include <QPainter>
#include <QColor>

class GraphicObject : public SmartObject
{
public:
    GraphicObject();
    GraphicObject(QColor color);
    ~GraphicObject();

    virtual vector<QString> print();
    virtual vector<QString> raw();
    virtual void draw(QOpenGLWidget* widget);
    virtual void mousePress2D(double x, double y, QMouseEvent* e);
    virtual void mouseMove2D(double x, double y, QMouseEvent* e);

    void drawText(QOpenGLWidget* widget, QString line, Point p);
    void drawText(QOpenGLWidget* widget, QString line, Point p, QColor color);
    void drawLine(QOpenGLWidget* widget, Point start, Point end);
    void drawLine(QOpenGLWidget* widget, Point start, Point end, QColor color);
    void drawSphere(QOpenGLWidget* widget, float radius, Point p);
    void drawSphere(QOpenGLWidget* widget, float radius, Point p, QColor color);

    double getWidth();
    double getLeft();
    double getRight();

    double getHeight();
    double getBottom();
    double getTop();

    double getDistance();
    double getNear();
    double getFar();
    Point* getCenter();
    QColor getColor();
    void setColor(QColor color);
    void setStatus(int status);
    int getStatus();

protected:
    void setGraphicSize(Point p);
    void setGraphicSize(Point leftBottom, Point rightTop);
    void setGraphicSize(vector<Point> p);
    void adjustBorder(int SIDE, double delta);
    void adjustBorder(Point p);

private:
    double left;
    double right;
    double bottom;
    double top;
    double nr;
    double fr;
    Point center;
    GLuint base;
    QFont font;
    QColor color;
    int status;

    void sizeChanged();
};

#endif // GRAPHICOBJECT_H
