#include "point.h"

Point::Point(){
    //ctor
    id = 0;
    x = 0.0;
    y = 0.0;
    z = 0.0;
}

Point::Point(int id, double x, double y){
    this->id = id;
    this->x = x;
    this->y = y;
    z = 0.0;
}

Point::Point(int id, double x, double y, double z){
    this->id = id;
    this->x = x;
    this->y = y;
    this->z = z;
}

int Point::getID(){
    return id;
}

double Point::getMod(){
    return sqrt(pow(x,2)+pow(y,2)+pow(z,2));
}

bool Point::equals(Point* p){
    if(p->x == x)
        if(p->y == y)
            if(p->z = z)
                return true;

    return false;
}


Point Point::operator + (Point b){
    Point p = Point(id,x + b.x, y + b.y, z + b.z);

    return p;
}

Point Point::operator - (Point b){
    Point p = Point(id,x - b.x, y - b.y, z - b.z);

    return p;
}

Point Point::operator * (float f){
    Point p = Point(id, x * f, y * f, z * f);

    return p;
}

double Point::operator * (Point b){
    double p = (x * b.x)+( y * b.y)+( z * b.z);

    return p;
}

Point Point::operator / (float f){
    Point p = Point(id, x / f, y / f, z / f);

    return p;
}

bool Point::operator == (Point b){
    if(x == b.x)
        if(y == b.y)
            if(z == b.z)
                return true;

    return false;
}

void Point::operator = (Point b){
    id = b.getID();
    x = b.x;
    y = b.y;
    z = b.z;
}


Point::~Point(){
    //dtor
}
