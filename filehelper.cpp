#include "filehelper.h"

bool FileHelper::saveFile(vector<QString> text, QString fileName)
{
    FILE *file;
    file = fopen(fileName.toLatin1().data(),"w+");

    if(file == NULL)
        return false;

    for(int i = 0; i < text.size(); i++)
        fprintf(file,text[i].toLatin1().data());

    fclose(file);

    return true;

}
