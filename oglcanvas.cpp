#include "oglcanvas.h"

OglCanvas::OglCanvas(QWidget *parent) : QOpenGLWidget(parent)
{
    scale = 1.0;
    transform = new Point(0.0,0.0,0.0);
}

OglCanvas::~OglCanvas(){

}

void OglCanvas::initializeGL(){
    glClearColor(1.0,1.0,1.0,1.0);

    loaded = false;
}

void OglCanvas::paintGL(){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glOrtho(0.0,width,height,0.0,-1.0,1.0);
    if(loaded){
        glPushMatrix();
        glScalef(scale,scale,scale);
        glTranslatef(transform->x,transform->y,transform->z);
        graphicObject->draw(this);
        glPopMatrix();
    }

}

void OglCanvas::resizeGL(int w, int h){
    width = w;
    height = h;

    glViewport(0, 0, w, h);
}
void OglCanvas::purgeGraphicObect(){
    loaded = false;
    graphicObject = NULL;
}

void OglCanvas::setGraphicObject(GraphicObject* graphicObject){
    if(graphicObject == NULL)
        return;

    Point* center = graphicObject->getCenter();

    double w = graphicObject->getRight() - graphicObject->getLeft();
    double h = graphicObject->getTop() - graphicObject->getBottom();
    double d = graphicObject->getNear() - graphicObject->getFar();

    //smallest scaling fator
    vector<float> scales;

    //WIDTH SCALE
    if(w > 0)
        scales.push_back(width/w);
    else
        scales.push_back((float)1e+20);

    //HEIGHT SCALE
    if(h > 0)
        scales.push_back(height/h);
    else
        scales.push_back((float)1e+20);

    //DEPTH SCALE
    if(d > 0)
        scales.push_back(2.0/d);
    else
        scales.push_back((float)1e+20);

    scale = MathHelper::min(scales);

    transform->x = (-center->x);
    transform->y = (-center->y);
    transform->z = (-center->z);

    this->graphicObject = graphicObject;
    loaded = true;
}

void OglCanvas::mousePressEvent(QMouseEvent* event){
    if(!loaded)
        return;

    double x = graphicObject->getLeft() + (((double)event->x()/(double)width) * graphicObject->getWidth());
    double y = graphicObject->getBottom() + (((double)(event->y())/(double)height) * graphicObject->getHeight());

    graphicObject->mousePress2D(x,y,event);
    this->update();
}

void OglCanvas::mouseMoveEvent(QMouseEvent* event){
    if(!loaded)
        return;

    float rx = (width/(graphicObject->getWidth())) * 0.8;
    float ry = (height/(graphicObject->getHeight())) * 0.8;
    float r = (rx > ry)? ry : rx;

    Point p = Point(1,event->x(),event->y());
    Point* center = graphicObject->getCenter();
    p = p - Point(1,width*0.1,height*0.1);
    p = p / r;
    p = p + Point(1,center->x,center->y);

    double xR =event->x()/(double)width;
    double yR = event->y()/(double)height;

    if(xR < 0.1 || xR > 0.9)
        return;

    if(yR < 0.1 || yR > 0.9)
        return;

    graphicObject->mouseMove2D(p.x,p.y,event);
    this->update();
}
